const letters = [
    {letter : 'ئا', quxandurush : 'unda-munda'},
    {letter : 'ئە', quxandurush : 'unda-munda'},
    {letter : 'ب', quxandurush : 'unda-munda'},
    {letter : 'پ', quxandurush : 'unda-munda'},
    {letter : 'ت', quxandurush : 'unda-munda'},
    {letter : 'ج', quxandurush : 'unda-munda'},
    {letter : 'چ', quxandurush : 'unda-munda'},
    {letter : 'خ', quxandurush : 'unda-munda'},
    {letter : 'د', quxandurush : 'unda-munda'},
    {letter : 'ر', quxandurush : 'unda-munda'},
    {letter : 'ز', quxandurush : 'unda-munda'},
    {letter : 'ژ', quxandurush : 'unda-munda'},
    {letter : 'س', quxandurush : 'unda-munda'},
    {letter : 'ش', quxandurush : 'unda-munda'},
    {letter : 'غ', quxandurush : 'unda-munda'},
    {letter : 'ف', quxandurush : 'unda-munda'},
    {letter : 'ق', quxandurush : 'unda-munda'},
    {letter : 'ك', quxandurush : 'unda-munda'},
    {letter : 'گ', quxandurush : 'unda-munda'},
    {letter : 'ڭ', quxandurush : 'unda-munda'},
    {letter : 'ل', quxandurush : 'unda-munda'},
    {letter : 'م', quxandurush : 'unda-munda'},
    {letter : 'ن', quxandurush : 'unda-munda'},
    {letter : 'ھ', quxandurush : 'unda-munda'},
    {letter : 'ئو', quxandurush : 'unda-munda'},
    {letter : 'ئۇ', quxandurush : 'unda-munda'},
    {letter : 'ئۆ', quxandurush : 'unda-munda'},
    {letter : 'ئۈ', quxandurush : 'unda-munda'},
    {letter : 'ۋ', quxandurush : 'unda-munda'},
    {letter : 'ئې', quxandurush : 'unda-munda'},
    {letter : 'ئى', quxandurush : 'unda-munda'},
    {letter : 'ي', quxandurush : 'unda-munda'},
    // 全部字母逐一填写到此处
];

// 选择cards
const container = document.querySelector('.grid-container');

// 从letters里遍历字母和其他
letters.forEach(({letter, quxandurush})=>{
    // 创建div
    const card = document.createElement('div');
    // 赋值class选择器
    card.className = 'card';

    const lttr = document.createElement('h2');
    lttr.className = 'lttr';
    lttr.textContent = letter;
    card.appendChild(lttr);

    const quxan = document.createElement('p');
    quxan.className = 'description';
    quxan.textContent = quxandurush;
    card.appendChild(quxan);

    // card.textContent = letter;
    container.appendChild(card);
});


const clr = document.querySelectorAll('div.card');

//随机取颜色
const randomHex = () => `#${Math.floor(Math.random() * 0xffffff).toString(16).padEnd(6, "0")}`;
// 反转颜色
const invertColor = (hex) => {
    const r = parseInt(hex.slice(1, 3), 16);
    const g = parseInt(hex.slice(3, 5), 16);
    const b = parseInt(hex.slice(5, 7), 16);

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);

    return `#${((1 - min) * 255) + max.toString(16).padStart(2, "0")}`;
};
const invertedColor = () => invertColor(randomHex());

clr.forEach((clr)=>{
    clr.style.backgroundColor = randomHex();
});